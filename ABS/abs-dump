#!/usr/bin/env python3
# -*- python -*-
#
# CERN AFS Service 
#
#$Id: $
#
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Perform internal setup of the environment.
# This is a Copy/Paste logic which must stay in THIS file
def standardSetup():
   import sys, os.path
   # insert the path to cernafs based on the relative position of this scrip inside the service directory tree
   exeDir = os.path.abspath(os.path.normpath(os.path.dirname(sys.argv[0])))
   pythonDir = os.path.join(os.path.dirname(exeDir), 'python' )
   sys.path.insert(0, pythonDir)
   import cernafs.setup
   cernafs.setup.standardSetup(sys.argv[0]) # execute a setup hook

standardSetup()
del standardSetup
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

import cernafs.util
import cernafs.cf
import cernafs.script

import cernafs.ABS.core

from cernafs.ABS.core import COMPACT_DATE_FORMAT, VoldumpEntry, find_voldump_chain, ArchiveFailure, ABSException

from cernafs.ABS.archiveCTA import archive_initialize,archive_query,archive_streamdump_prepare_process, archive_streamdump_check_process, archive_streamdump_cleanup 

from cernafs import vos
import os

import random
import logging
import time
import sys


# if for some operational reasons you want to create backup snapshots but not the dumps, then set DO_ONLY_BACKUPSYS to True
DO_ONLY_BACKUPSYS=False

# Here is the outline of the dump server algorithm

# - delay the start such that all other parallel dump processes on other servers are distributed evenly within a time period (say 2 hours)
# - create backup snapshots for all volumes on this server (vos backupsys)
# - take note of those you could not do and retry them a certain number of times later
# - for each partition [in parallel up to the defined thread limit]
#     - dump and send backup snapshots one by one

# some remarks:

# vos rename keeps the volume id intact (so the id is the only real
# key) - however we should keep track also of the name, in case one
# wants to recover a deleted volume by name (for which the VLDB
# name/id is no longer available) - but maybe VLDB stores this for us
# somewhere

# if we store both vid:name then the name may change if volume is
# renamed, hence grep on name is not valid (grep on vid)


force_full_tagdir="/afs/cern.ch/project/afs/var/ABS/force_full"

def is_tagged_force_full(v):
   import os
   tags = []
   try:
      tags = os.listdir(force_full_tagdir)
   except OSError:
      pass
   return str(v) in tags

def remove_tag_force_full(v):

   # protect against malicious filenames
   assert(not v.startswith(".")) 
   assert(not v.startswith("/")) 

   import os
   try:
      os.unlink(os.path.join(force_full_tagdir,str(v)))
   except OSError:
      pass

##############################################################################################
# This is a high-level dump functionality which could be possibly placed in a separate module

class ArchiveObjectHandle(cernafs.util.DataStruct):
   """ Handle to the object sent to archive:
       - name       : fully qualified name of the object in the archive
       - arch_bytes : archived object size (different from src_bytes if compression or encryption enabled)
       - src_bytes  : raw object size (CURRENTLY NOT SUPPORTED)
       - elapsed_seconds : time for the command to run
   """
   pass


class VosDumpFailure(ArchiveFailure): 
   pass

import subprocess

def vos_dump_to_archive(volheader,fn,data_encoding,from_time=None):
    """ Dump a volume <volheader.backupID> and send it to archive as
    the filename <fn>. If <from_time> is not given or is 0 then do a
    full dump, otherwise make an incremental dump based on
    <from_time>.  The data_encoding parameter is a symbolic name which
    specifies the format in which the volume dump is produced (for
    example, it could be compressed).

    Return 
Return the path to the dumped
    file (note: it may be modified depending on the encoding).

    <from_time> is specified as a datetime object and is internally used with a resolution of one minute.

    Supported data encodings:
     * "None" : raw vos dump
     * "gzip" : gzipped, .gz extension added to the file name
     * "aes**": encrypted, see source code

    Raises VosDumpFailure in case of errors. This is used for flow control (retries,etc).
    """
    if not from_time:
        from_time = 0
    else:
        from_time = from_time.strftime(US_DATE_FORMAT)

    
    for enc in data_encoding.split('.'):
       if enc not in ['None','gzip'] and not enc.startswith('aes'):
          raise ValueError('Unsupported data encoding (%s)'%repr(enc))

    log_fn = os.path.join(logdir,'dump_to_archive.%s'%volheader.id)

    def logfile_path(name):
       return log_fn + "." + name

    id = volheader.backupID

    t0 = time.time()

    process = []

    def spawn_piped_subprocess(c,name,last=False,env=None):
       """ Create and return a subprocess object connecting stdin-stdout via pipes.
       
           - c is a shell command to run
           - name uniquily identifies a process in the pipeline, it is also used to create a name of logfile for stderr
           - last process in the pipeline does not have stdout redirected anywhere
      
       """
       logger.debug("Executing subprocess %s, stderr piped to %s, stdout passed on to the next process",c,logfile_path(name))

       if process:
          stdin = process[-1].stdout
       else:
          stdin = None

       if not last:
          stdout = subprocess.PIPE
       else:
          stdout = None

       p = subprocess.Popen(c,stderr=open(logfile_path(name),'w'), stdout = stdout, stdin=stdin, shell=True,env=env)
       p.name = name
       p.cmd = c
       p.logfile = logfile_path(name)
       process.append(p)

       return p

    # VOSDUMP PROCESS
    spawn_piped_subprocess('vos dump -id %s -time "%s" -verbose -localauth'%(id,from_time),'vosdump')

    # DATA ENCODING PROCESSES
    # each data transformation should add a suffix to the filename which will be then stripped at restore to get raw volume dump data

    for enc in data_encoding.split("."):
       logger.debug("FN: %s",fn)
       if enc == "gzip":
           fn += '.gz'
           spawn_piped_subprocess('gzip -v -',"gzip")
           continue

       if enc.startswith('aes'):
           fn += '.'+enc
           spawn_piped_subprocess('openssl enc -e -aes-256-cbc -pass file:/var/ABS/private/pw.%s -md md5'%enc,'openssl')
           continue

       logger.error('Ignoring unknown data encoding: %s',enc) # should never hit this because the unknown data encodings should have been already detected by the code above


    logger.debug("FN: %s",fn)

    # ARCHIVE PROCESS prepare the dump 

    archive_command, archive_process_name, archive_command_environ = archive_streamdump_prepare_process(volheader.id,fn)

    # create the dump subprocess as defined by the dump context
    archive_process = spawn_piped_subprocess( archive_command, 
                                           archive_process_name, 
                                           last=True, 
                                           env = dict(list(os.environ.items()) + list(archive_command_environ.items())) )
    

    # EXECUTE THE PIPELINE
    for p in process[:-1]:
       p.stdout.close() # Allow processes to receive a SIGPIPE if next process exits.

    # do execute all processes
    process[-1].communicate()

    # collect return codes
    retcode = [p.poll() for p in process]

    # PROCESS ERRORS (if any)
    # try to be smart and find the real source of error in the piped command chain...

    for p,rc in zip(process,retcode):

       if rc is None: # the process has not finished yet, which most certainly means a broken pipe due to next process(es) terminating prematurely
          logger.warning("process %s not finished yet - broken pipe, check next process error, stderr in %s",repr(p.cmd),repr(p.logfile))
          continue

       # always check for archive errors independently of exitcode
       # this allows to integrate properly archive commands which do not have consistent exitcode
       if p.name == archive_process_name:
          # a ArchiveFailure or VosDumpFailure exception should be raised in case of errors
          try:
             arch_bytes = archive_streamdump_check_process(rc,p.logfile,volheader.id,fn)
             continue
          except ArchiveFailure as x:
             archive_streamdump_cleanup(volheader.id,fn)
             raise

       # FIXME: does not cover a case of all None, or one None and rest 0
       if rc != 0:

          if cernafs.util.find_string_in_file("Broken pipe",file(p.logfile)):
             logger.warning("process %s broken pipe, check next process error, stderr in %s",repr(p.cmd),repr(p.logfile))
             continue

          logger.warning("non-zero exit code of %s, rc=%s, stderr in %s",repr(p.cmd),rc,repr(p.logfile))

          if p.name == 'vosdump':
             pass

#          if p.name == archive_process_name:
#
#             # allow the archive backend to influence the flow control (by specifying the action)
#             code,action,description = archive_streamdump_get_error_code(p.exitcode,p.logfile)
#             
#             if code:
#                raise VosDumpFailure("%s: %s %s"%(p.name,code,description),action)

       
          # this is the default action (unknown failure)
          archive_streamdump_cleanup(volheader.id,fn)
          raise VosDumpFailure("failed to execute %s, stderr in %s"%(repr(p.cmd),repr(p.logfile)),"SKIP")


    # no errors, well done, prepare the return data (output filename and statistics)
    elapsed_t = time.time()-t0

    # retrieve the transfer size and other details
    # ...and pack them into the ArchiveObjectHandle

    # removed src_bytes as it makes no sense to handle it here
    # vos exa size is a better approximation of the process in this context and may be done externally to this function
    rh = ArchiveObjectHandle(name=None,arch_bytes=arch_bytes,elapsed_seconds=elapsed_t)

    #if not rh.name:
    #   logger.warning("The archive filename not reported by the archive backend (%s)",archive_process.logfile)

    if not rh.arch_bytes:
       logger.warning("The archived bytes not reported by the archive backend (%s)",archive_process.logfile)

    extra_msg = ""
    if data_encoding == "gzip":
       try:
          rh.compression_rate = float(open('%s.gzip'%log_fn).readline().strip()[:-1]) #strip the % character
          #if rh.compression_rate>0:
          #   rh.src_bytes = rh.arch_bytes/(1-rh.compression_rate/100.0)
          extra_msg += ", data compression rate %s" % rh.compression_rate
       except IOError as x:
          logger.warning("problem parsing %s.gzip (extraction of compression rate %s)"%(log_fn,x))

    logger.info("[volumeid=%s] name %s, archived %.1f KB, elapsed %.2f seconds%s",volheader.id,volheader.name, rh.arch_bytes/1000., elapsed_t, extra_msg)

    return rh

###################################################################################################

import datetime
US_DATE_FORMAT = "%m/%d/%Y %H:%M" # AFS legacy format expected by some commands, e.g. vos dump (NOTE: minute resolution)

###################################################################################################

total_completed_sizeKB = 0 # for all threads

parallel_partitions_semaphore = None # initalized in main()

def guard_parallel_run(f):
   """ Don't let f() run in more than N threads at the time, N = config_options['max_parallel_partitions']
   """
   def wrapper(*args,**kwds):
      if config_options.get('max_parallel_partitions',0):
         try:
            parallel_partitions_semaphore.acquire()
            return f(*args,**kwds)
         finally:
            parallel_partitions_semaphore.release()
   return wrapper

def log_unhandled_exceptions(f):
   def wrapper(*args,**kwds):
      try:
         return f(*args,**kwds)
      except Exception as x:
         if logger:
            logger.critical(x)
         else:
            print("CRITICAL: %s (warning: no logger)"%str(x), file=sys.stderr)
         raise
   return wrapper

@log_unhandled_exceptions
# @guard_parallel_run #: throttling here delays backupsys for partitions which wait for their turn
def main_dump(server,part,mode="passive",only_volumes=None,rejected_volumes=None,test=False):
   """

   If mode=="active" then make backup snapshot and dump volumes from server/partition, unless only_volumes list is provided.

   If mode == "passive" then use existing backup snapshots and dump volumes from server/partition, unless only_volumes list is provided.

   The mode=="force_full" is like active mode but all volumes on a
   given partition will be made with full backup.

   The only_volumes list give the volumes to be dumped

   test flag specifies if the test should be run

   """

   logger.info("[servpart=%s%s] Partition sequence begin, mode=%s",server,part,mode)

   # CREATE BACKUP VOLUME SNAPSHOTS

   # backupsys produces a list of volumes to be backed up; while the
   # backup is running a backup volume snapshot determined in this
   # step may cease to exist at a later stage due to a deletion or
   # move of the RW volume master; this error condition should be
   # reported but probably not corrected (correction would require the
   # backup process on another machine to actively detect this which
   # may be difficult as the backup processes do not finish at the
   # same time and the constency scanning may be problematic)

   
   backupsys_logfile = os.path.join(logdir,"vos_backupsys.%s.%s"%(server, part.replace('/', '')))


   if mode == "passive":

      # meaning of passive: don't make backup snapshots -- use the snapshots which already exist (created by someone else)

      backup_vols = [v.name for v in cernafs.vos.listvol(server,part) if cernafs.ABS.core.is_backupable(v)]
      
      # restrict the list to the volumes specified by only_volumes
      if only_volumes:
         backup_vols = list(set(backup_vols).intersection(set(only_volumes)))

         if len(only_volumes) != len(backup_vols):
            logger.info("[servpart=%s%s] trimmed volume list (keeping only RW volumes): diff %d",server,part, len(only_volumes)-len(backup_vols))

      logger.info("[servpart=%s%s] vos.listvol: total %d",server,part, len(backup_vols))

   else:
      assert(mode in ["active","force_full"])
      if only_volumes:
         # backup volumes specified by only_volumes
         backupsys_entries = vos.backupsys(server,part,volumes=only_volumes,rejected_volumes=rejected_volumes,cache=True,cache_path=backupsys_logfile,cache_auto_fn=False)
      else:
         # backup all volumes on the partition
         backupsys_entries = vos.backupsys(server,part,rejected_volumes=rejected_volumes,cache=True,cache_path=backupsys_logfile,cache_auto_fn=False)


      if not backupsys_entries:
         logger.info("[servpart=%s%s] vos.backupsys: nothing to backup",server,part)
         return

      backup_vols = [e.rw_volname for e in backupsys_entries if e.success]

      # here are the failed entries (with error message)
      # to be handled somehow (FIXME)
      failed_entries = [e for e in backupsys_entries if not e.success]

      # FIXME: what to do if backupsys fails completely?
      # this could indicate a serious problem on the server itself - hence maybe further actions should be abandoned (?)

      n_failed = len(failed_entries)
      n_total = len(backupsys_entries)

      logger.info("[servpart=%s%s] vos.backupsys: total %d failed %d",server,part, n_total,n_failed)

      # until error recovery is in place report all failures

      if n_failed:
         logger.error("[servpart=%s%s] volumes failed at backupsys (n_failed=%d, n_total=%d)",server,part,n_failed,n_total)

      for e in failed_entries:
         logger.error("[servpart=%s%s] backupsys failed for %s (BK id=%s)",server,part,e.rw_volname,e.bk_volid)

      # with error recovery in place, we may simply resort to a warning in specific cases...

      #if n_failed > 100 or (n_total and n_failed/float(n_total) > 0.1): # more than 10% failures
      #   logger.warning("[servpart=%s%s] unusually high number of volumes failed at backupsys (n_failed=%d, n_total=%d)",server,part,n_failed,n_total)

      # FIXME: error recovery for later
      # redo_queue_backup = queue(failed_vols)

      # if for some operational reasons you want to create backup snapshots but not the dumps, then set DO_ONLY_BACKUPSYS to True
      if DO_ONLY_BACKUPSYS:
         return 

   @guard_parallel_run # delay the sequential dump in case more than max threads are already dumping...
   def sequential_volume_dump():

      # SEQUENTIALLY DUMP THE VOLUMES

      logger.info('[servpart=%s%s] starting sequential dump of %d volumes',server,part,len(backup_vols))

      # this is a testing hook
      if test:
         pre_dump_cmd = config_options['test'].get('pre_dump_cmd',None)

         if pre_dump_cmd:
            logger.info('[servpart=%s%s] executing testing hook pre_dump_cmd %s',server,part,repr(pre_dump_cmd))
            os.system(pre_dump_cmd)

      global total_completed_sizeKB

      sequence_completed_sizeKB = 0
      start_time = time.time()

      
      today = datetime.datetime.now().date() 

      if today < config_options['active_phasein_begin']:
         logger.info('We have not yet entered the active phasein period... skipping all volumes')
         return 

      for volnum, vname in enumerate(backup_vols):

         if all_threads_stop:
            logger.info('[servpart=%s%s] Interrupting sequence at (%d/%d) completed',server,part,volnum, len(backup_vols))
            break
         try:
            logger.debug('[servpart=%s%s] Processing volume %s (%d/%d)', server, part, vname, volnum, len(backup_vols))

            try:
               volheader = vos.examine(vname) # get volume header
            except Exception as x:
               logger.error('failed to vos examine %s, skipping volume, details: %s',vname,x)
               continue

            def event_log(event,**kwds):
               logger.info('[volumeid=%s] %s',volheader.id, ', '.join([event] + ["%s=%s"%(k,repr(v)) for k,v in zip(list(kwds.keys()),list(kwds.values()))]))

            def event_log_error(event,**kwds):
               logger.error('[volumeid=%s] %s',volheader.id, ', '.join([event] + ["%s=%s"%(k,repr(v)) for k,v in zip(list(kwds.keys()),list(kwds.values()))]))

            event_log('[servpart=%s%s] processing_volume'%(server,part), name=volheader.name, sizeKB=volheader.diskused)


            # ACTIVE PHASE-IN
            if today <= config_options['active_phasein_end']:
               active_period_spread = (config_options['active_phasein_end'] - config_options['active_phasein_begin']).days + 1
               active_volume_day = int(cernafs.util.uniform_hash(volheader.id)*active_period_spread)

               logger.info('active_period_spread %d, active_volume_day %d', active_period_spread, active_volume_day)
               if (today-config_options['active_phasein_begin']).days < active_volume_day:
                  event_log('[servpart=%s%s] INACTIVE'%(server,part), name=volheader.name)
                  continue


            # CONSISTENCY CHECKS
            #
            # check for "ghost volumes"
            #print (volheader.serv.replace('.cern.ch',''),volheader.part.replace('/vicep',''))
            #print (server,part)
            if not (cernafs.util.same_hostname(volheader.serv,server) and cernafs.util.same_partition(volheader.part,part)):
               event_log_error("Volume moved? Server/partition in VLDB does not match the current server/partition... dump skipped")
               continue


            # DECIDE IF FULL BACKUP OR AN INCREMENTAL
            # 
            #   analyze the backup chain for the volume and get last time for incremental 

            previousBackupDate = None # full dump by default, otherwise a value will indicate the incremental dump (based on the previousBackupDate)

            most_recent_chain = []
            #TEST: disabled

            try:
               archive_dumps = archive_query(volheader.id)
            except ArchiveFailure as x:
               # FIXME: for the moment treat RETRY_LATER and SKIP as "SKIP"
               if x.action == "ABORT":
                  event_log_error("Aborting dump sequence",server=server,part=part,error=str(x))
                  raise
               logger.info("[volumeid=%s] cannot query archive, skipping volume... %s",volheader.id,str(x))
               continue


            try:
               most_recent_chain = find_voldump_chain(archive_dumps)
            except ABSException as x:
               logger.error('forced to do a full dump due to the catalog error: %s',str(x))
               most_recent_chain = None

            if most_recent_chain:

               previous_dump = most_recent_chain[0]

               if previous_dump.backupDate == volheader.backupDate:
                  logger.warning("Backup dump of %s on %s already exists. Previous backup dump for this volume exists with the same backup date. Skipping the volume dump, otherwise it would reference itself. \n previous_dump=%s \n volheader=%s",volheader.name, volheader.backupDate, previous_dump,volheader)
                  continue

               def do_full_if_fixed_backup_cycle(full_backup_cycle_duration):
                  # full backup cycle
                  current_backup_cycle_timespan = datetime.datetime.now()-most_recent_chain[-1].backupDate

                  if current_backup_cycle_timespan < full_backup_cycle_duration:
                     logger.info("[volumeid=%s] current backup cycle timespan %s (max allowed full backup cycle timespan %s)",volheader.id,current_backup_cycle_timespan,full_backup_cycle_duration)
                     logger.info("[volumeid=%s] decision: will do incremental dump as of %s for %s based on previous dump date %s",volheader.id,volheader.backupDate,volheader.name,previous_dump.backupDate)
                     return False
                  else:
                     return True


               def do_full_if_hashed_absolute_cycle(volid):
                  SPREAD = config_options['backup_cycle_spread'] # days
                  SMEAR_FUNCTION = config_options['backup_cycle_smear_function'] 
                  reference_date = datetime.datetime(2013,0o1,0o1)

                  full_cycle_day = int(cernafs.util.uniform_hash(volid)*SPREAD)

                  day_number = (datetime.datetime.now()-reference_date).days

                  logger.info("[volumeid=%s] hashed absolute cycle: cycle_day_number: %d, full_cycle_day: %d",volid,day_number%SPREAD,full_cycle_day)
                  if day_number%SPREAD == full_cycle_day:
                     logger.info("[volumeid=%s] decision: will do a full backup based on hashed absolute cycle",volid)
                     return True

                  # option additional security if missed a full backup: avoid too long cycles
                  # randomize the maximum cycle limit -- otherwise there would be wave effects in case of a missed night
                  return do_full_if_fixed_backup_cycle(datetime.timedelta(days=SPREAD+SMEAR_FUNCTION()))


               if mode == "force_full":
                  logger.info("[volumeid=%s] decision: forced full backup",volheader.id)
               else:
                  if not do_full_if_hashed_absolute_cycle(volheader.id): 
                     previousBackupDate = previous_dump.backupDate  # do an incremental


            if is_tagged_force_full(volheader.name):
                   logger.info("[volumeid=%s] decision: forced full backup by admin tag",volheader.id)
                   previousBackupDate = None
                

            if not previousBackupDate:
               logger.info("[volumeid=%s] decision: will do full backup for %s",volheader.id, volheader.name)
            else:
               # Optimization: do not do the incremental if volume was not updated since last dump
               if volheader.updateDate < previous_dump.backupDate:
                  # there was no update in the volume, hence we may skip the dump altogether
                  logger.info("[volumeid=%s] volume not updated since last dump, skipping... updateDate=%s, last backupDate=%s",volheader.id, volheader.updateDate, previous_dump.backupDate)
                  continue

            # DECIDE ON THE DATA ENCODING
            # this is simply a configuration parameter but it could also be derived from the volume type or historical information on the volume contents
            # it should be possible to change data_encoding for a volume at any time (so a backup chain could consist of dumps with different encodings)

            try:
               data_encoding = config_options['data_encoding']
            except KeyError:
               data_encoding = "None"

            #data_encoding = "None" 
            #data_encoding = "gzip" # symbolic value representing the data encoding used to store the file, should not contain the ":" character or whitespaces


            if previousBackupDate:
               if most_recent_chain:
                  chainId = most_recent_chain[-1].backupDate.strftime(COMPACT_DATE_FORMAT)
               else:
                  logger.critical('Unable to establish the chainId for %s',volheader.name)
                  continue
            else:
               chainId = volheader.backupDate.strftime(COMPACT_DATE_FORMAT)
                        

            # PREPARE THE VOLUME DUMP

            voldump = VoldumpEntry(id=volheader.id,name=volheader.name,backupDate=volheader.backupDate,previousBackupDate=previousBackupDate,dataEncoding=data_encoding,chainId=chainId)

            dump_try_count = 0
            ARCHIVE_DUMP_MAX_TRY = config_options.get('ARCHIVE_DUMP_MAX_TRY',3)

            dump_exception = None

            while dump_try_count < ARCHIVE_DUMP_MAX_TRY:
            
               dump_try_count += 1

               try:
                  rh = vos_dump_to_archive(volheader,voldump._filename(),data_encoding,from_time=previousBackupDate)
                  sequence_completed_sizeKB += rh.arch_bytes/1000.
                  total_completed_sizeKB += rh.arch_bytes/1000.

                  event_log('vos_dump_to_archive_completed', elapsed_time=rh.elapsed_seconds,apparent_throughput=rh.arch_bytes/1000./rh.elapsed_seconds)

                  # NOTE: calculating change_rate needs source_bytes which is currently not supported
                           # change_rate=rh.src_bytes/1000./volheader.diskused


                  # remove the full tag (if exists) for successful dumps
                  if is_tagged_force_full(volheader.name):
                     remove_tag_force_full(volheader.name)

                  break 

               #except cernafs.util.CachedCallError,x:
               #   event_log_error('vos_dump_to_archive_failed',exc=str(x))
               #   continue
               except ArchiveFailure as x:

                  dump_exception = x

                  if x.action == 'RETRY':
                     event_log('RETRY: ',dump_try_count=dump_try_count, exc=str(x))
                     time.sleep(30) # wait some time for any transient reason to go away... slow down to protect the volserver and/or archive from an avalanche of requests
                     continue

                  if x.action == 'SKIP':
                     event_log_error('vos_dump_to_archive_failed, skipping...',exc=str(x))
                     time.sleep(0.3) # slow down to protect the volserver and/or archive from an avalanche of requests
                     break

                  if x.action == 'RETRY_LATER':
                     event_log_error('RETRY_LATER not implemented for now, skipping...',exc=str(x))
                     time.sleep(0.3) # slow down to protect the volserver and/or archive from an avalanche of requests
                     break

                  if x.action == 'ABORT':
                     event_log_error("Aborting dump sequence",server=server,part=part,error=str(x))
                     raise

                  event_log_error('CRITICAL: invalid error action code %s'%repr(x.action))
                  raise

            if dump_exception and dump_exception.action in ['SKIP','RETRY_LATER']:
               continue # skip this dump altogether
            

         finally:
            sequence_elapsed_time = time.time()-start_time+0.1
            logger.info('[servpart=%s%s]: status after %s/%s processed volumes: sequence_completed_sizeKB=%.2f sequence_elapsed_seconds=%.2f sequence_apparent_throughput=%.2f total_completed_sizeKB=%.2f',server,part,volnum+1, len(backup_vols), sequence_completed_sizeKB, sequence_elapsed_time, sequence_completed_sizeKB/sequence_elapsed_time, total_completed_sizeKB)

      logger.info("[servpart=%s%s] Partition sequence end",server,part)

   # actually invoke the sequential volume dump
   sequential_volume_dump()

###################################################################################################

backup_session_id = None
logdir = None
logger = None

import threading
all_threads_stop = False
threads = []

config_options = {}

@log_unhandled_exceptions
def main():
   # ---- command line options -----

   from optparse import OptionParser
   parser = OptionParser(usage="""%prog [options] operation_mode 

Start the backup dump session for the local host.

Operation mode may be:

   * active  - make backup snapshots and dump all volumes
   * passive - dump volumes using existing backup snapshots
   * force_full - force full dump and backup snapshot of all volumes on all partitions
   * centrally_managed - the operation mode is defined by a tag in afsadmin.cf

""")
   parser.add_option("--config", action="store", default=None, help="location of the configuration file (defaults to %s)"%cernafs.script.rundir("abs.config"))
   parser.add_option("--test",action="store_true",default=False, help='enable test mode as defined in the configuration file')
   parser.add_option("--dry-run",action="store_true",default=False, help='do not perform the backup, only print what would be done')
   parser.add_option("--verbose",action="store_true",default=False, help='be verbose, otherwise just print warnings and errors')
   parser.add_option("--random_sleep",action="store",default=None, type="int", help='how long to sleep before starting')

   (options, args) = parser.parse_args()

   try:
      operation_mode = args[0]
   except IndexError:
      parser.print_help()
      print("ERROR: not enough arguments", file=sys.stderr)
      return -1

   if not operation_mode in ["active","passive","force_full","centrally_managed"]:
      parser.print_help()
      print("ERROR: unknown operation_mode", file=sys.stderr)
      return -1      

   if options.dry_run:
      print("ERROR: dry_run not yet implemented", file=sys.stderr)
      return -1
      
   # ---- initialize logging ----
   global backup_session_id, logdir, logger

   dt_start = datetime.datetime.now()

   backup_session_id = datetime.datetime.now().strftime(COMPACT_DATE_FORMAT)
   logdir = '/var/ABS/log/abs-dump-session.%s'%backup_session_id
   try:
      os.makedirs(logdir)
   except OSError:
      logdir = '/tmp/ABS/log/abs-dump-session.%s'%backup_session_id
      os.makedirs(logdir)
      print("WARNING: logging redirected to /tmp", file=sys.stderr)
   os.chdir(logdir) # writable place, in case some logs are written to cwd
   loglevel = logging.ERROR
   if options.verbose:
      loglevel = logging.INFO
   cernafs.script.setupFileLogging(loglevel,logfile_path=os.path.join(logdir,'abs-dump'))
   logger = cernafs.script.getMainLogger()

   logger.info("Starting backup session... logfile %s",cernafs.script.logfile)

   # ---- config options ----
   global config_options
   config_options = cernafs.ABS.get_config_options(options.config)

   if not config_options.get('enabled',True):
      logger.info("Disabled in the config file. Stop.")

   if config_options.get('force_quiet',False) and not options.verbose:
      logger.info("Forcing quiet mode (set from config file).")
      cernafs.script.overrideConsoleLogLevel(logging.CRITICAL)
      

   server = cernafs.util.hostname()
   partitions = cernafs.vos.listpart(server)

   # ---- command line options -----

   #operation_mode = "active"
   #operation_mode = "passive"
   #operation_mode = "phasein"
   #operation_mode = "force_full"

   if operation_mode == "centrally_managed":

      logger.info("Running as centrally_managed")

      this_server = cernafs.cf.server_features(require=[server.split(".")[0]],prod=False)
   
      try:
         this_server = this_server[0]
      except IndexError:
         logger.critical("Server %s not defined in afsadmin.cf and thus does not appear centrally managed, bailing out",server)
         return

      for tag in this_server.tags:
         op_mode = tag.split(':')
         if len(op_mode) > 1 and op_mode[0] == 'abs-dump':
            operation_mode = op_mode[1] # (the validity of the operation_mode is checked on the fly)
            
      if operation_mode == 'centrally_managed':
         operation_mode = config_options['default_centrally_managed_mode']
         if operation_mode:
            logger.info("Applying default operation mode defined by abs.config")

      if not operation_mode:
         logger.critical("Undefined operation mode tag in afsadmin.cf for %s (and the tag is required by abs.config): the tag should be of form abs-dump:<mode> where <mode> is one of the regular operation modes expected as argument to abs-dump command",server)
         return 0

   if not options.test:
      random_sleep = 0
      if options.random_sleep is not None:
         random_sleep = options.random_sleep
      elif config_options['random_sleep']:
         random_sleep = config_options['random_sleep']
      if random_sleep:
         seconds = random.uniform(0,random_sleep)
         logger.info("Sleeping now for %d seconds",seconds)
         time.sleep(seconds)

   # -------------------------------

   logger.info("Operation mode: %s",operation_mode)   

   logger.debug("All partitions on server %s: %s",server,partitions)

   #partitions = ['aa','ab','ac','ad','ae','af','ag']

   if options.test:
      partitions = list(set(partitions).intersection(set(config_options['test']['partvols'].keys())))

   #partitions = ['ag','ab']
   #partitions = ['aa','ab']

   #if TEST_RUN == 'server':
   #   partitions.remove('ab') # leave out 'ab'

   #if TEST_RUN == 'small':
   #   partitions = ['aa']
      
   if options.test:
      logger.info("TEST RUN ENABLED: %s",repr(config_options['test']))

   mode = dict([(p,operation_mode) for p in partitions])

   # special treatment of phasein mode
   # the actual mode for each partition is computed based on the phase-in date stored in the schedule file
   if operation_mode == "phasein":
      today = datetime.datetime.now().date()
      schedule_fn = '/var/ABS/phasein.schedule'
      active_date = {}
      try:
         logger.info('Reading the phasein schedule from %s ...',schedule_fn)
         for line in open(schedule_fn):
            line = line.strip()
            if not line: # skip blank lines
               continue
            fields = line.split(' ')
            p = fields[0]
            d = ' '.join(fields[1:])
            d = eval(d)
            active_date[p] = d
      except IOError as x:
         logger.info('Schedule file not found, creating a new schedule in %s ...',schedule_fn)
         active_date = dict([(p,today+datetime.timedelta(days=n)) for n,p in enumerate(partitions)])
         f = open(schedule_fn,'w')
         for p,d in zip(list(active_date.keys()),list(active_date.values())):
            f.write(p + " " + repr(d) + "\n")
         f.close()
      mode = {}
      for p in active_date:
         logger.info('Partition %s active on %s',p,active_date[p])
         if today<active_date[p]:
            mode[p] = "passive"
         else:
            mode[p] = "active"

   #if TEST_RUN == 'server':
   #   partitions = ['aa']
   #   mode[0] = "force_full"

   # another test: force full on all partitions
   #mode = ["force_full" for x in partitions]

   unprocessed_partitions = set(partitions) - set(mode.keys())
   
   if unprocessed_partitions:
      logger.critical('The following partitions have no backup schedule and hence will not be processed: %s',unprocessed_partitions)

   # setup the archive backend
   try:
      archive_initialize(config_options,logdir)
   except cernafs.util.CachedCallError as x:
      print(x.result.stdout.read())
      raise
   
   global parallel_partitions_semaphore
   max_parallel_partitions = config_options.get('max_parallel_partitions',0)
   if max_parallel_partitions:
      logger.info('Max parallel partitions set to %s',max_parallel_partitions)
      parallel_partitions_semaphore = threading.BoundedSemaphore(max_parallel_partitions)

   rejected_volumes = []
   rejected_file = config_options.get('rejected_volumes', None)
   if rejected_file:
      try:
         f = open(rejected_file, 'r')
         rejected_volumes = f.read().split()
         f.close()
      except IOError:
         logger.info("Rejected volumes file not found, skipping")

   #return 
   for p in mode:
      only_volumes = None
      if options.test:
         only_volumes = config_options['test']['partvols'][p]
      t = threading.Thread(target=main_dump,args=(server,p,mode[p],only_volumes,rejected_volumes), kwargs={'test':options.test})
      threads.append(t)
      t.start()

   for t in threads:
      t.join()


   dt_elapsed = datetime.datetime.now() - dt_start
   logger.info("Ending backup session... session elapsed time %f seconds (%s), logfile %s",cernafs.util.datetime_total_seconds(dt_elapsed),dt_elapsed,cernafs.script.logfile)

###################################################################################################

main()

###################################################################################################

