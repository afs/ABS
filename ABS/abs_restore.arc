#------------------------------------------------------------
#
# CERN IT/DIS 1998, AFS administration
#
# File        : 
# Description : arc procedure for restoring volumes from the new backup system (NBS)
# Author      : Kuba, 2013
#
#------------------------------------------------------------

$ACL{'abs_restore'}="ANY";

sub abs_restore {
    &safeargs($args);
    my $volume_name = $args;

    # cd to a writable place so that a client might write its log
    use File::Temp qw/ tempfile tempdir /;
    $dir = tempdir( CLEANUP => 1 );
    chdir $dir;

    # call the real restore program
    system("exec_as_admin-ad /opt/ABS/ABS/abs-restore $volume_name");

    chdir "/tmp"; # to make it possible to clean the temp dir

    exit(1);
}
