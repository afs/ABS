import subprocess,sys,time,io, pickle, os


class CachedCallResult:
    """ Result attributes of a `cached_call`:
    
         - `pid`, `returncode`: meaning the same as for `subprocess.Popen` objects
         - `cache_used`: indicate if the result was read from cache
         - `stdout`,`stderr`: output files open for reading
    """
    def __init__(self):
        self.pid = None
        self.returncode = 0
        self.cache_used = False
        self.stdout = None
        self.stderr = None

class CachedCallError(Exception):
    """Indicates that a `cached_call` command returned a non-zero exit code. The `result` attribute (:class:`CachedCallResult`) gives access to further details.
    """
    def __init__(self,args,result):
        self.cmdargs = args # FIXME: python bug? self.args seems to be converted automatically to a tuple (may it be a clobbering property?)
        self.result = result

    def __str__(self):
        cmd = str(self.cmdargs)
        try:
            cache_file = self.result.stdout.name
        except AttributeError:
            cache_file = "<memory>"
        errmsg_stderr=self._peek_stderr()
        errmsg_stdout=self._peek_stdout()
        return  '"%s" returned %s (pid=%s,cache_used=%s,cache_file=%s) stdout: %s stderr: %s' % (cmd,self.result.returncode,self.result.pid,self.result.cache_used,cache_file,errmsg_stdout,errmsg_stderr) 

    def _peek_stderr(self):
        try:
            self.result.stderr.seek(-1024, 2) # SEEK_END = 2
        except IOError:
            self.result.stderr.seek(0) # if file is smaller than 1024 then go to the beginning
        errmsg = self.result.stderr.read()
        self.result.stderr.seek(0) # rewind for future use
        return errmsg

    def _peek_stdout(self):
        try:
            self.result.stdout.seek(-1024, 2) # SEEK_END = 2
        except IOError:
            self.result.stdout.seek(0) # if file is smaller than 1024 then go to the beginning
        errmsg = self.result.stdout.read()
        self.result.stdout.seek(0) # rewind for future use
        return errmsg
        
def cached_call(args,cache=None,cache_path=None,cache_auto_fn=None, cache_expired=False, silent=None, check_exception=True, **subprocess_opts):
    """ Run commands (with optional output caching in an external file) and return the
    stdout/stderr as file objects open for reading.

    Caching is disabled by default and entire content of stdout/stderr
    is read directly into memory. You may enable caching for two reasons:

       * output is too large to fit in memory so must be stored in a file
       * output is expensive or time consuming to produce so you may reuse it on a subsequent call

    If command execution succeeds and its exit code is zero, then
    :class:`CachedCallResult` object is returned. In case of
    non-zero exit code, the :class:`CachedCallError` exception is
    raised, which contains the `result` object as well. In both
    cases the `stdout`,`stderr` file objects are open for reading.
    The `result.returncode` contains the exit code of the executed
    command.

    The underlying mechanism is based on :class:`subprocess.Popen`. If
    the command may not be executed at all, then any exception raised by
    :class:`subprocess.Popen`, typically `OSError`, is propagated back to the caller of
    `cached_call`.  The `subprocess_opts` may be used to pass additional
    options to :class:`subprocess.Popen`.
    
    The args may be a string or a list - just like in
    :class:`subprocess.Popen`.  If args is a list then all items will
    be automatically converted to strings (using :func:`str`).

    If not `silent` (default) then stderr output of the subprocess is
    also logged by the `cached_call.logger` (at the `WARNING` level).

    The :class:`CachedCallError` exceptions may be disabled by setting
    `check_exception` to `False`.  If disabled, then the `cached_call`
    always returns :class:`CachedCallResult` even if the command
    returns a non-zero exit code.

    Caching may be enabled with `cache=True`. In this case the output
    is stored in the cache file on disk instead.  On a subsequent call
    if cache is present and not expired, the corresponding
    stdout/stderr file objects are simply returned and the
    `call_result.cache_used` is set to `True`. The `cache_expired`
    option is by default set to False (so the cache never expires).

    The cache files are located in `/tmp`, and by default their names
    are based on the command args.  Thus if exactly the same command
    (including arguments and options) is run twice by two different
    processed on the same host, the second call will simply read
    output from cache. The `cache_path` may be used to specify
    arbitrary cache path and file basename. If `cache_auto_fn` is True
    (default), then a string derived from the command args is
    automatically appended to the cache path. Otherwise, the cache
    path is left as is.

    The defaults for caching options may changed globally by setting
    the cached_call attributes (to be used with care in multithreaded
    or library code):

      - `cached_call.CACHE_PATH`: default location of the output cache
      - `cached_call.CACHE`: default caching mode (if not explicitly specified by the `cached_call` keyword argument)
      - `cached_call.SILENT`: default slient mode 
      - `cached_call.CACHE_AUTO_FN`: default mode for automatic suffix in cache path

    For example, to turn on default caching globally for all `cached_call()` invocations simply set 
    `cached_call.CACHE=True`. 

    In addition, if customized logger is needed, then it may be set as `cached_call.logger`.
    """

    if cached_call.logger is None:
        import logging
        cached_call.logger = logging.getLogger("python.cernafs.util.cached_call")

    from cernafs.util import isStringLike

    if cache is None:
        cache = cached_call.CACHE

    if silent is None:
        silent = cached_call.SILENT

    if cache_auto_fn is None:
        cache_auto_fn = cached_call.CACHE_AUTO_FN


    if not isStringLike(args):
        args = [str(x) for x in args]

    proc = None
    result = None

    try:
        if cache:
            if not cache_path:
                cache_path = cached_call.CACHE_PATH
            
            if cache_auto_fn:
                if isStringLike(args):
                    args_path = args
                else:
                    args_path = '_'.join(args)
                args_path = args_path.replace(os.sep, '__')
                cache_path += '_'+args_path

                #FIXME: at time the name is too long...

                # make sure that the filename does not exceed 255 characters (typical limit on filesystems: ext3, ext4, xfs, ...)
                #head,tail = os.path.split(cache_path)
                #
                #max_filename = 255-7 # 7 takes into account appended names in the code below (_stderr,...)
                #if len(tail)>max_filename:
                    

            cache_stderr = cache_path+'_stderr'
            cache_stdout = cache_path+'_stdout'
            cache_result = cache_path+'_result'

            if not cache_expired:
                try:
                    cached_call.logger.debug('cached_call: %s reading output from cache %s,%s',repr(args),cache_stderr,cache_stdout)
                    result = pickle.load(open(cache_result))
                    result.cache_used = True
                    result.stdout = open(cache_stdout)
                    result.stderr = open(cache_stderr)
                    return result
                except (EOFError,IOError) as x:
                    cached_call.logger.debug('cached_call: %s cache read failed (%s), preparing write cache',repr(args),x)
            else:
                cached_call.logger.debug('cached_call: %s cache expired, preparing write cache',repr(args))
            
            # open cache files for writing to be subsequently passed onto Popen call
            stdout = open(cache_stdout,'w')
            stderr = open(cache_stderr,'w')
            result_file = open(cache_result,'wb')
        else:
            cached_call.logger.debug('cached_call: %s, caching disabled, using memory buffers for stdout/stderr',repr(args))
            # indicate internal, in-memory pipes to be used for output
            stdout = subprocess.PIPE
            stderr = subprocess.PIPE

        cached_call.logger.debug('cached_call: executing %s',repr(args))

        proc = subprocess.Popen(args, stdout=stdout, stderr=stderr, encoding='UTF-8',**subprocess_opts)

        ##############################################
        # memory buffer for PIPEs (to avoid deadlock)
        #
        import fcntl

        membuf_stdout = ""
        membuf_stderr = ""

        # make non-blocking
        if not cache:
            for f in [proc.stdout,proc.stderr]:
                fd = f.fileno()
                fl = fcntl.fcntl(fd, fcntl.F_GETFL)
                fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)

        def non_block_read(output):
            try:
                return output.read() or ""
            except:
                return ""
        #
        #
        ##############################################

        while proc.poll() is None:
            if not cache:  # read output chunk by chunk to avoid deadlock when using PIPEs
                membuf_stdout += non_block_read(proc.stdout) 
                membuf_stderr += non_block_read(proc.stderr) 
            time.sleep(0.05)

        cached_call.logger.debug('cached_call: completed %s, returncode=%d',repr(args),proc.returncode)

        ##############################################
        ## final handling of memory buffers for PIPEs
        # now, we are done with the process, we make the output pipes blocking 
        # so that subsequent reads would not throw IOError 11: Resource temporarily unavailable (EAGAIN)

        if not cache:
            for f in [proc.stdout,proc.stderr]:
                fd = f.fileno()
                fl = fcntl.fcntl(fd, fcntl.F_GETFL)
                fcntl.fcntl(fd, fcntl.F_SETFL, fl & ~(os.O_NONBLOCK))
        #
        ############

        result = CachedCallResult()
        result.pid = proc.pid
        result.returncode = proc.returncode
        result.cache_used = False

        if cache: # open cache files for reading
            pickle.dump(result,result_file)
            result.stdout = open(cache_stdout)
            result.stderr = open(cache_stderr)
        else:
            # read-in the reminder of output (if any)
            membuf_stdout += proc.stdout.read() or ""
            membuf_stderr += proc.stderr.read() or ""
            # convert result.std* into memory string-like objects (for the non silent mode which requires seek())
            result.stdout = io.StringIO(membuf_stdout)
            result.stderr = io.StringIO(membuf_stderr)

            # in-memory pipes should be open for reading already
            #result.stdout = proc.stdout
            #result.stderr = StringIO.StringIO(proc.stderr.read()) # let's copy the output from pipe to a memory buffer (for the non silent mode which requires seek())

        return result

    finally:

        # handle stderr output unless Popen raised an exception
        if not result is None and not silent:
            if result.stderr:
                msg = result.stderr.read()
                if msg:
                    cached_call.logger.warning('stderr: %s',msg)
                result.stderr.seek(0) # rewind to the beginning for reading again
            else:
                cached_call.logger.warning('missing stderr object')

        # raise exception on non-zero return codes (if enabled and no exception from Popen)
        if result and check_exception:
            if result.returncode != 0:
                raise CachedCallError(args,result)


# default value of 'cache' optional argument of cached_call() function
cached_call.CACHE=False

# default value of 'cache_path' optional argument of cached_call() function
cached_call.CACHE_PATH='/tmp/afs_cache'

# default value of 'cache_auto_fn' optional argument of cached_call() function
cached_call.CACHE_AUTO_FN=True

# default mode is verbose (propagate errors)
cached_call.SILENT=False

# default logger
cached_call.logger=None

if __name__ == '__main__':

    def assert_file_empty(f):
        s = f.read().strip()
        if s:
            print('stderr:',s, file=sys.stderr)
            assert(0 != "stderr is not empty")

    def assert_file_nonempty(f):
        assert(f.read().strip())

    def assert_normal_result(r):
        assert_file_empty(r.stderr)
        assert_file_nonempty(r.stdout)

    # test all combinations, multiple times
    for cache in [True,False]:
        for silent in [True,False]:
            for cache_expired in [True,False]:
                for i in range(5):

                    try:
                        r = cached_call(['ls','/non/existing/file/123456'],cache=cache,silent=silent,cache_expired=cache_expired)
                    except CachedCallError as x:
                        str(x) # try to format exception to string
                    else:
                        assert(not "CachedCallError expected")

                    try:
                        r = cached_call(['/non/existing/file/123456'],cache=cache,silent=silent,cache_expired=cache_expired)
                    except OSError as x:
                        pass
                    else:
                        assert(not "OSError expected")

                    assert_normal_result( cached_call('ls', cache=cache,silent=silent,cache_expired=cache_expired) )
                    assert_normal_result( cached_call(['ls','-a'],cache=cache,silent=silent,cache_expired=cache_expired) )
                    assert_normal_result( cached_call('ls -a',shell=True,cache=cache,silent=silent,cache_expired=cache_expired) )

