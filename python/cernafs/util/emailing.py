afsadmin_address = "afs-admins@cern.ch"

def send_email_to_admins(subject, body):
    send_email(subject, body, afsadmin_address)

def send_email(subject,body,to_address,from_address=None):
    from cernafs.util import hostname
    from email.MIMEText import MIMEText
    import smtplib
    import getpass

    if from_address is None:
        from_address = getpass.getuser()+"@"+hostname()

    msg = MIMEText(body)
    msg['Subject'] = subject
    msg['From'] = from_address
    msg['To'] = to_address

    server = smtplib.SMTP()
    server.connect()
    #server.set_debuglevel(1)
    
    server.sendmail(msg['From'], [ msg['To'] ], msg.as_string())
    server.quit()
    
