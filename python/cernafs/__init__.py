import time



# this is a workaround for a wierd bug in multithreaded scripts:
# AttributeError: _strptime http://bugs.python.org/issue11108 

# this should be called in the main thread, so it is put here in hope
# that cernafs is imported somewhere near the program beginning

time.strptime("3 Aug 75", "%d %b %y")
