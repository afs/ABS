
"""
Interface to vos commands.

Example usage::

  >>> for p in vos.listpart('afs108'):
  ...    print p, sum([v.sizeKB for v in vos.listvol('afs108',p) if v.name and v.name[0] in ['/vicepq','/viceps']])/1024./1024.
   b 154.509589195
   c 260.041460991
   d 215.590883255
   e 292.481509209
   f 221.171380043
   g 262.226537704
   h 214.474150658
   i 159.367405891
   j 336.694796562
   k 227.23085022
   l 338.797628403
   m 283.453014374
   n 235.3145895
   o 148.527907372
   s 0.000127792358398
   


Note that ``v.name`` may be empty if volume is busy. The last condition may be replaced by a slice: ``[.... if v.name[0:1] in ['/vicepq','/viceps']]``

Another example::

 >>> for s in cf.servers():
 ...   for p in vos.listpart(s):
 ...     print 'server/partition',s,p
  server/partition afs10 /vicepb
  server/partition afs10 /vicepc
  server/partition afs10 /vicepd
  ...
  server/partition afs48 /vicepcm
  server/partition afs48 /vicepcu
  server/partition afs48 /vicepdu
  ...


"""

from cernafs.util import cached_call, DataStruct
import re
import logging
import datetime

def listpart(server,**cached_call_opts):
    """ Run ``vos listpart`` and return parsed output as in :func:`parse_listpart`.
    """
    r = cached_call(['vos','listpart',server],**cached_call_opts)
    return parse_listpart(r.stdout)

def listvol(server,partition='',**cached_call_opts):
    """ 
    Run ``vos listvol`` for a specified server/partition and
    return a list of volume entries :class:`VolumeShortEntry` as in :func:`parse_listvol`.

    If partition is not specified, return volume lists for all partitions
    in a dictionary ``{partition_name : volume_entry_list}``.

    """
    command = ['vos','listvol','-server',server]

    if partition:
        command.extend(['-partition',partition])

    r = cached_call(command,**cached_call_opts)
    listvol = parse_listvol(r.stdout)
    
    if partition:
        return listvol[partition]
    else:
        return listvol


def examine(vol_name_or_id,**cached_call_opts):
    """ Run ``vos examine`` and return :class:`VolumeHeader` as in :func:`parse_examin_format`.
    """

    command = ['vos','examine','-format',vol_name_or_id]

    r = cached_call(command,**cached_call_opts)
    return parse_examine_format(r.stdout)


class VolumeHeader(DataStruct):
    """ Volume header as reported by vos examine.

Provides the attributes produced by "vos exa -format", ids and flags are kept as strings, counters are converted to ints, dates are converted to datetime objects. Example:
      * name		user.moscicki
      * id		536925159
      * serv            afs130.cern.ch
      * part		/vicepch
      * status		OK
      * backupID	536925161
      * parentID	536925159
      * cloneID		0
      * inUse		Y
      * needsSalvaged	N
      * destroyMe	N
      * type		RW
      * creationDate	857400117	Mon Mar  3 15:41:57 1997
      * accessDate	1353486697	Wed Nov 21 09:31:37 2012
      * updateDate	1353486641	Wed Nov 21 09:30:41 2012
      * backupDate	1353432289	Tue Nov 20 18:24:49 2012
      * copyDate	1320683539	Mon Nov  7 17:32:19 2011
      * flags		0	(Optional)
      * diskused	1154700
      * maxquota	10485760
      * minquota	0	(Optional)
      * filecount	24981
      * dayUse		3305
      * weekUse		84689	(Optional)
      * spare2		0	(Optional)
      * spare3		0	(Optional)

Additional attributes:

      * serv_ip		137.138.4.161	      <----- (serv attribute originally has two values)
      * LOCKED          True if locked, False otherwise
    """
    def _init_attrs(self):
        """ Set all known attributes to None.        
        """
        for name in all_vos_examine_attributes:
            setattr(self,name,None)


test_examine_format_s = """name		user.moscicki
id		536925159
serv		137.138.4.161	afs130.cern.ch
part		/vicepch
status		OK
backupID	536925161
parentID	536925159
cloneID		0
inUse		Y
needsSalvaged	N
destroyMe	N
type		RW
creationDate	857400117	Mon Mar  3 15:41:57 1997
accessDate	1353490071	Wed Nov 21 10:27:51 2012
updateDate	1353489921	Wed Nov 21 10:25:21 2012
backupDate	1353432289	Tue Nov 20 18:24:49 2012
copyDate	1320683539	Mon Nov  7 17:32:19 2011
flags		0	(Optional)
diskused	1154710
maxquota	10485760
minquota	0	(Optional)
filecount	24981
dayUse		3644
weekUse		84689	(Optional)
spare2		0	(Optional)
spare3		0	(Optional)

user.moscicki 
    RWrite: 536925159     Backup: 536925161 
    number of sites -> 1
       server afs130.cern.ch partition /vicepch RW Site 
    RWrite: 536925159     Backup: 536925161 
    number of sites -> 1
       server afs130.cern.ch partition /vicepch RW Site 

"""

all_vos_examine_attributes = set("""id
serv
part
status
backupID
parentID
cloneID
inUse
needsSalvaged
destroyMe
type
creationDate
accessDate
updateDate
backupDate
copyDate
flags
diskused
maxquota
minquota
filecount
dayUse
weekUse
spare2
spare3
""".splitlines())


def parse_examine_format(f):
    """ Parse "vos exa -format" output and return  :class:`VolumeHeader`.
    """

    def mk_datetime(s):
        return datetime.datetime.fromtimestamp(float(s))

    conv_type = {'creationDate': mk_datetime, 
                 'accessDate': mk_datetime, 
                 'updateDate': mk_datetime, 
                 'backupDate': mk_datetime, 
                 'copyDate': mk_datetime, 
                 'maxquota':int,
                 'minquota':int,
                 'diskused':int,
                 'filecount':int,
                 'dayUse':int,
                 'weekUse':int
                 }

    attrs = {'LOCKED':False}

    section = 1

    for line in f:
        if not line.strip() or section == 2: # a first blank line separates the well-formatted section 1 from the remaining free-text information (section 2)
            section = 2
            if "Volume is currently LOCKED" in line:
                attrs['LOCKED'] = True      
        else:
            fields = line.split()

            key = fields[0]

            if key == 'serv': # break off server into two attributes
                attrs['serv_ip'] = fields[1]
                attrs['serv'] = fields[2]
                continue

            value = fields[1]

            #print repr(key),repr(value)

            try:
                value = conv_type[key](value)
            except KeyError:
                pass

            attrs[key] = value


    # check for missing attributes (this is important as the client code may assume that all attrs are defined)
    missing_attrs = all_vos_examine_attributes - set(attrs.keys())

    if missing_attrs:
        msg = "missing attributes '%s' in vos examine output '%s'"%(missing_attrs, [line for line in f])
        logging.warning(msg)
        raise ValueError(msg)


    return VolumeHeader(**attrs)
    
    

test_listpart_s = """The partitions on the server are:
    /viceps    /vicepah    /vicepai    /vicepbh    /vicepbi    /vicepfh 
   /vicepfi 
Total: 7
"""



def parse_listpart(f):
    """ Parse vos listpart output and return a list of full partition names
    """
    part = []

    for line in f:

        if line and line[0]=='T': # effectively skip the first and last line of input, these lines start with the T character!
            continue

        for p in line.split():
            part.append(p)

    return part



test_listvol_s = """Total number of volumes on server afs120 partition /viceps: 14 
p.afs.old                         537281894 RW     188172 K On-line
p.afs.old.backup                  537281896 BK     188172 K On-line
p.afs.perlcore                    537248694 RW     107817 K On-line
p.afs.perlcore.backup             537248696 BK     107817 K On-line
p.afs.samba                       536973466 RW      19174 K On-line
p.afs.samba.backup                536973468 BK      19174 K On-line
q.afs.15a.AA                     1934242206 RW     434646 K On-line
q.afs.afs120.s                   1933859592 RW         67 K On-line
q.afs.afs155.ba                  1934192343 RW         67 K On-line
q.afs.afs155.be                  1934192355 RW         67 K On-line
q.afs.afs156.ar                  1934197550 RW         67 K On-line
q.afs.t.afs35.9                   537474437 RW         34 K On-line
q.afs.t.lxs5018.10                537474338 RW    1048610 K On-line
q.afs.t.opp26.0                   537470709 RW     790562 K On-line

Total volumes onLine 14 ; Total volumes offLine 0 ; Total busy 0

Total number of volumes on server afs120 partition /vicepae: 152 
p.atlas.Ana160602                1934230429 RW     985546 K On-line
p.atlas.Ana160602.backup         1934230431 BK     985546 K On-line
q.atlas.sw.FCT-v1                1933833372 RW   10275533 K On-line
**** Volume 1934309955 is busy ****
**** Volume 1934309957 is busy ****
**** Could not attach volume 537093198 ****

Total volumes onLine 3 ; Total volumes offLine 0 ; Total busy 2

"""

class VolumeShortEntry(DataStruct): 
    """Short entry with volume status summary:

         * name (string, empty if volume busy)
         * id (string)
         * type (string): "RO","RW","BK"
         * sizeKB (int)
         * status (string): "On-line"
    """
    pass

def parse_listvol(f):
    """ Parse ``vos listvol`` output and return a dictionary mapping the partition name to the list of volume entries (:class:`VolumeShortEntry`).
    Busy volumes are included in the list, however their names are empty (and all other details, except for their ids are empty. 
    
    Example::

     >>> volparts = parse_listvol(test_listvol_s) # test_listvol_s is a test string defined in this module
     >>> v = volparts['s'][0]
     >>> print volparts.keys()
      ['/viceps','/vicepae']
     >>> print len(volparts['/viceps'])
      14
     >>> print v.id 
      '537281894'
     >>> print v.sizeKB
      188172
    """ 
    vols = {}

    part = None

    for line in f:
        
        line = line.strip()

        if not line: # skip empty lines, which also means that the volume listing for current partition has ended
            part = None
            continue

        r=re.match(r"Total number of volumes on server (\S+) partition (/vicep\w+):.*",line)
        if r:
            serv,part = r.groups()
            vols.setdefault(part,[])
            continue

        if part:
            r = re.match(r"\s*\*{4} Volume (\d+) is busy \*{4}",line)

            if not r:
                r = re.match(r"\s*\*{4} Could not attach volume (\d+) \*{4}",line)

            if not r:
                r = re.match(r"\s*\*{4} (.+) \*{4}",line)
                if r:
                    msg = "unrecognized special output: %s" % repr(line)
                    logging.error(msg)
                    raise ValueError(msg)

            if r:
                entry=['',r.group(1),'',0,None,'']
            else:
                entry=line.split()
                assert(entry[4]=='K') # make sure units are always reported as kilobytes

            vols[part].append(VolumeShortEntry(name=entry[0],id=entry[1],type=entry[2],sizeKB=int(entry[3]), status=entry[5]))
            continue

        if re.match(r"^Total volumes onLine",line): continue # ignore summaries

        logging.warning('unrecognized input: %s',line)

    return vols


test_backupsys_s = """Backing up volumes on server afs72 .. 
Creating backup volume for user.moscicki._R8 on Tue Dec  4 16:54:38 2012
Re-cloning backup volume 1934540276 ... done

Creating backup volume for p.svn.svnstats._R2 on Tue Dec  4 16:54:38 2012
Creating a new backup clone 1934540277 ...Failed to clone the volume 1934535382
Volume is off line
Could not backup p.svn.svnstats._R2

Creating backup volume for p.svn.svnstats._R1 on Tue Dec  4 16:54:38 2012
Re-cloning backup volume 1934540278 ... done

Creating backup volume for q.afs.afs72.a on Tue Dec  4 16:54:38 2012
Re-cloning backup volume 1934500812 ... done

Creating backup volume for q.afs.afs72.b on Tue Dec  4 16:54:38 2012
Re-cloning backup volume 1934500815 ... done

Creating backup volume for q.afs.afs72.c on Tue Dec  4 16:54:39 2012
Re-cloning backup volume 1934500818 ... done

Creating backup volume for q.afs.afs72.s on Tue Dec  4 16:54:39 2012
Re-cloning backup volume 1934500821 ... done

Creating backup volume for user._R on Tue Dec  4 16:54:39 2012
Re-cloning backup volume 1934540279 ... done

Creating backup volume for q.afs.afs72.a._R2 on Tue Dec  4 16:54:39 2012
Re-cloning backup volume 1934540280 ... done

Creating backup volume for q.afs.afs72.a._R1 on Tue Dec  4 16:54:39 2012
Creating a new backup clone 1934540281 ...Failed to clone the volume 1934518933
Volume is off line
Could not backup q.afs.afs72.a._R1

Creating backup volume for q.afs.afs72.a._R on Tue Dec  4 16:54:39 2012
Re-cloning backup volume 1934540282 ... done

Creating backup volume for q.afs.tsm6.test1._R3 on Tue Dec  4 16:54:39 2012
Re-cloning backup volume 1934540283 ... done

Creating backup volume for q.afs.tsm6.test1._R4 on Tue Dec  4 16:54:39 2012
Re-cloning backup volume 1934540284 ... done

Creating backup volume for q.afs.tsm6.test1._R5 on Tue Dec  4 16:54:39 2012
Re-cloning backup volume 1934540285 ...Could not re-clone backup volume 1934540285
Volume is off line
Could not backup q.afs.tsm6.test1._R5

Creating backup volume for q.afs.afs200.s._R on Tue Dec  4 16:54:39 2012
Creating a new backup clone 1934540286 ...Failed to clone the volume 1934536001
Volume is off line
Could not backup q.afs.afs200.s._R

Creating backup volume for q.afs.afs200.s._R1 on Tue Dec  4 16:54:39 2012
Re-cloning backup volume 1934540287 ... done

Creating backup volume for q.afs.tsm6.test1._R6 on Tue Dec  4 16:54:39 2012
Re-cloning backup volume 1934540288 ... done

Creating backup volume for q.afs.tsm6.test1._R7 on Tue Dec  4 16:54:39 2012
Re-cloning backup volume 1934540289 ... done

Creating backup volume for q.afs.tsm6.test1._R8 on Tue Dec  4 16:54:39 2012
Re-cloning backup volume 1934540290 ... done

Creating backup volume for p.afs.Tu.536912932 on Tue Jan 22 16:38:52 2013
Could not lock the VLDB entry for the volume 1934568826
VLDB: vldb entry is already locked
Could not backup p.afs.Tu.536912932

Creating backup volume for p.afs.Tw.1934435585 on Wed Jan 23 10:01:02 2013
Could not start a transaction on the volume 1934567110
VOLSER: volume is busy
Could not backup p.afs.Tw.1934435585

done
Total volumes backed up: 15; failed to backup: 5
"""

# vos backupsys return 0 if ALL volumes were backed up OK
# it return -2 if some failed to backup some volumes 

# when the VLDB entry is locked then here are the symptomps:

# <stdout>
# Creating backup volume for p.afs.Tu.536912932 on Tue Jan 22 16:38:52 2013
# <stderr>
# Could not lock the VLDB entry for the volume 1934568826
# VLDB: vldb entry is already locked
# <stdout>
# Could not backup p.afs.Tu.536912932

# when the volserver is busy then here are the symptomps:

#
# Creating backup volume for p.afs.Tw.1934435585 on Wed Jan 23 10:01:02 2013
# Could not start a transaction on the volume 1934567110
# VOLSER: volume is busy
# Could not backup p.afs.Tw.1934435585


class VosBackupsysStatusEntry(DataStruct):
   """ Result of a backupsys per volume:
     * rw_volname: RW volume name
     * bk_volid: BK volume id (this may be left as None if parsing of a particular error message has not been implemented)
     * success: True if backup volume created successfully
     * errormsg: list of error messages if not success
   """
   pass

def parse_vos_backupsys_verbose_output(stdout_and_stderr):

   result_entries = []

   re_create = re.compile("Creating backup volume for (?P<volname>\S+) on (?P<date>.+)")
   re_result = re.compile("(Creating a new backup clone|Re-cloning backup volume) (?P<volid>\d+) [.]{3}\s?(?P<result>.+)") # volid "..." and an optional whitespace
   re_result_error = re.compile("Could not backup (?P<volname>\S+)") # special result case for locked VLDB entries

   re_total = re.compile("Total volumes backed up")

   re_ignore_list = [
       re.compile("Omitting to backup (\S+) since the RW is in a different location")
       ]

   # header is everything up to the first [re_create] match
   header = True
   current_volentry = None
   error_mode = False
   expect_result = False # expect the result to be present on the next line, if not present then we enter error_mode automatically

   for line in stdout_and_stderr:
      line = line.strip()

      if not line:
         continue

      if line == 'done':
         # in 1.6 the done line appears for every volume (and not only at the end)
         # just ignore it
         continue

      if re_total.search(line): # end of file
         #print line 
         if current_volentry:
            result_entries.append(current_volentry)
         #print 'entries',len(result_entries)
         break

      m = re_create.search(line)
      
      if m:
         header = False
      else:
         if header: continue

      if m:
         if current_volentry:
            result_entries.append(current_volentry)

         current_volentry = VosBackupsysStatusEntry(rw_volname=m.group('volname'),errormsg=[],bk_volid=None)
         error_mode = False
         expect_result = True
         continue

      m = re_result.search(line)

      if m:
         #print "RESULT FOUND",line
         current_volentry.bk_volid = m.group('volid')

         if m.group('result') == 'done':
            current_volentry.success = True
            expect_result = False
            error_mode = False
         else:
            current_volentry.success = False
            current_volentry.errormsg = [m.group('result')]
            error_mode = True
            expect_result = False
         continue

      #print '***',line,expect_result,error_mode

      if expect_result:
         current_volentry.success = False
         error_mode = True
         expect_result = False

      m = re_result_error.search(line)

      if m:
         error_mode = False
         continue

      if error_mode:
         current_volentry.errormsg.append(line)
      else:
         just_ignore = False 
         for re_ign in re_ignore_list:
             if re_ign.search(line):
                 just_ignore = True
                 break
         if not just_ignore:
             logging.error("vosbackupsys: unrecognized output line %s",repr(line))

   return result_entries

#for e in parse_vos_backupsys_verbose_output(s_backupsys_test1.splitlines()):
#   print e
#sys.exit(2)

from .ABS.core import backupsys_xprefix

def backupsys(server,partition,volumes=None,rejected_volumes=[],**cached_call_opts):
    """ Run a backupsys command with -verbose  option and return: 
          - a list of sucesfully backed up volume names (R/W)
          - a list of failed volume names (R/w)

       If a list of volumes is specified then backup only those listed by name.
    """

    volprefix = ""
    rejected = backupsys_xprefix

    if volumes == []:
       logging.debug("Empty list of volumes passed to vos_backupsys")
       return []
    
    if volumes:
       volprefix = "-prefix "+" ".join(volumes)
    else:
        volumes = []

    if rejected_volumes:
        rejected += " "
        rejected += " ".join([vol for vol in rejected_volumes if vol not in volumes])


    cmd = "vos backupsys -xprefix %s %s -server %s -partition %s -verbose" % (rejected,volprefix,server,partition)

    logging.debug(cmd)
    
    #return [] # FIXME: TEST for now

    #result = cernafs.util.cached_call(cmd, shell=True, cache_expired=True, cache=True, cache_path='/tmp/ABS/vos_backupsys.%s'%backup_session_id,cache_auto_fn=True)
    #entries = parse_vos_backupsys_verbose_output(result.stdout)
    
    r = cached_call(cmd + " 2>&1",shell=True,**cached_call_opts)

    entries = parse_vos_backupsys_verbose_output(r.stdout)

    return entries

    #outfn = os.path.join(logdir,"vos_backupsys.%s.%s.%s"%(backup_session_id,server,partition))
    #os.system(cmd + "> %s 2>&1" % outfn)
    #print outfn
    #entries = parse_vos_backupsys_verbose_output(file(outfn))
    #return entries





if __name__ == '__main__':

    import sys

    # when executing this script one may pass a test file explicitly
    test_files = sys.argv[1:]
    
    import os
    print('API test: begin')
    print(parse_examine_format(test_examine_format_s.splitlines()))
    parse_listpart(test_listpart_s.splitlines())
    parse_listvol(test_listvol_s.splitlines())
    
    for e in parse_vos_backupsys_verbose_output(test_backupsys_s.splitlines()):
        #try to access attributes
        print(e.rw_volname, end=' ')
        print(e.bk_volid, end=' ')
        print(e.success, end=' ')
        print(e.errormsg)  

    # these files contain a captured output of vos backupsys
    # and in principle they are supposed to parse without any backup failures detected

    test_files += ['test_vos_backupsys_1.txt','test_vos_backupsys_16server.txt'] #,'test_vos_backupsys_2.txt']

    problem_detected = False

    for fn in test_files:
        print('***',fn)
        vollist = parse_vos_backupsys_verbose_output(open(os.path.join(os.path.dirname(os.path.abspath(__file__)),fn)))
        print(len(vollist))
        for e in vollist:
            if not e.success:
                print("Problem with",e)
                problem_detected = True

    assert(not problem_detected)

    print('API test: end')
