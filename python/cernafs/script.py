""" High-level utilities for service management scripts.

This module provides implementation for certain high-level patterns useful for executable scripts for service management.

The rundir() works correctly only if this module is imported before anything else changes the current working directory.

"""

from cernafs.util import send_email_to_admins, hostname
import traceback
import getpass
import os.path
import sys

logfile = None
_rundir = None

def rundir(path=""):
    """ Runtime directory of the main script. 
    """
    global _rundir
    if _rundir is None:
        _rundir = os.path.abspath(os.path.dirname(sys.argv[0]))
    return os.path.join(_rundir,path)

rundir() # store the rundir (important if sys.argv[0] is a relative path and someone changes cwd later)

def setupFileLogging(console_level, filemode='a',pid=False,logfile_path=None, logfile_level=None):
    import logging
    global logfile

    logfile = logfile_path

    if not logfile:
        logfile = os.path.join('/tmp',os.path.basename(sys.argv[0]))

    if pid:
        logfile += "."+str(os.getpid())
    logfile += ".log"

    if not logfile_level:
        logfile_level = logging.DEBUG

    root = logging.getLogger('')

    # log everything to the file
    logging.basicConfig(level=logfile_level, 
                        format='%(asctime)s %(name)+12s>> %(levelname)-8s: %(message)s',
                        filename=logfile,
                        filemode=filemode) 

    # and to the console only at the specified level (with a simpler format)
    console = logging.StreamHandler()
    console.setLevel(console_level)
    formatter = logging.Formatter('%(asctime)s %(levelname)-8s: %(message)s')
    console.setFormatter(formatter)
    root.addHandler(console)
    root.consoleHandler = console # attribute for easy level override in the client code

    # report error via syslog

    import logging.handlers
    syslog = logging.handlers.SysLogHandler(address='/dev/log')
    syslog.setLevel(level=logging.ERROR)
    root.addHandler(syslog)

    return getMainLogger()

def overrideConsoleLogLevel(level):
    import logging
    root = logging.getLogger('')
    root.consoleHandler.setLevel(level)

def getMainLogger():
    import logging
    return logging.getLogger("main.%s"%os.path.basename(sys.argv[0]))

def format_info_msg(summary,details):
    try:
        username = getpass.getuser()
    except Exception:
        username = "<unknown>"

    assert(the_main)
    script_file = os.path.abspath(_func_globals(the_main)['__file__'])

    return  """
===============
Summary    : %s
Executable : %s
Run as     : %s@%s
Logfile    : %s
===============

%s"""%(summary, script_file, username,hostname(),logfile,details)

the_main = None

def _func_globals(f): # compatibility function for older python versions
    try:
        return f.__globals__
    except AttributeError: 
        return f.__globals__
   
def run(main,*args,**kwds):
    """ Wrap invocation of the `main` function and send email to afs-admins in case of abnormal termination (i.e. exception).
    Use::

    def main():
      ...
    if __name__=="__main__":
       run(main)

    The return value of main() does not matter, if you want to terminate with a code, use sys.exit(code).

    """

    global the_main
    assert(the_main is None)
    the_main = main

    script_file = os.path.abspath(_func_globals(main)['__file__'])

    try:
        main(*args,**kwds)
    except SystemExit as x: # for older python versions
        if x.code != 0:
            subject = "non-zero exit status [%d]: %s"%(x.code,script_file)
            msg = format_info_msg(subject,traceback.format_exc())
            getMainLogger().critical(msg)
            send_email_to_admins(subject,msg)
        raise
    except Exception as x:
        subject = "execution problem: %s"%script_file
        msg = format_info_msg("%s:%s"%(x.__class__.__name__,str(x)),traceback.format_exc())
        getMainLogger().critical(msg)
        send_email_to_admins(subject,msg)
        raise

if __name__ == "__main__":

    import cernafs.util.emailing
    import os
    # don't bother the entire mailing list with the test...
    cernafs.util.emailing.afsadmin_address = os.getlogin() + "@cern.ch"

    import logging
    setupFileLogging(logging.INFO)

    try:
        main = sys.argv[1]
    except IndexError:
        print("test not run: a main function must be specified as an argument", file=sys.stderr)
        sys.exit(1)

    print('test',main)

    #IMPORTANT: when adding new tests here, make sure to update the run-tests driver!

    if main == "main1":

        def main():
            pass

        try:
            run(main)
        except SystemExit as x:
            assert(x.code is None)
        except Exception:
            assert(0) # should not happen

    if main == "main2":
        
        def main():
            import sys
            sys.exit(0)

        try:
            run(main)
        except SystemExit as x:
            assert(x.code == 0)
        except Exception:
            assert(0) # should not happen

    if main == "main3":

        def main():
            import sys
            sys.exit(1)

        try:
            run(main)
        except SystemExit as x:
            assert(x.code == 1)
        except Exception:
            assert(0) # should not happen


    if main == "main4":

        def main():
            raise Exception("test")

        try:
            run(main)
        except SystemExit as x:
            assert(0) # should not happen
        except Exception:
            pass

