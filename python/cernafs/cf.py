#
# CERN AFS Service
#

""" 
Interface to query afsadmin.cf file which is a definitive database holding information about servers, partitions and projects used in CERN AFS.

Top-level functions most convenient to use:
  - :func:`partition_features`
  - :func:`server_features`
  - :func:`servers`

For example, print names of all servers in production for which "backup" tag is defined:
   >>> from cernafs import cf
   >>> print cf.servers(require=["backup"])
   ['afs10', 'afs11', 'afs12', 'afs21', 'afs22', 'afs23', 'afs24', 'afs25', 'afs26', 'afs27', 'afs36', 'afs37', 'afs38', 'afs39', 'afs41', 'afs42', 'afs43', 'afs44', 'afs45', 'afs46', 'afs47', 'afs48', 'afs100', 'afs102', 'afs104', 'afs106', 'afs108', 'afs110', 'afs120', 'afs121', 'afs122', 'afs123', 'afs124', 'afs125', 'afs126', 'afs130', 'afs131', 'afs132', 'afs133', 'afs134', 'afs135', 'afs136', 'afs137', 'afs140', 'afs141', 'afs150', 'afs151', 'afs152', 'afs153', 'afs154', 'afs155', 'afs156', 'afs157']

Print all pools which are assigned to partitions except those which have a X_no_warranty tag:

  >>> print set(sum([p.pools for p in cf.partition_features(exclude=["X_no_warranty"])],[]))
  set(['alice_p', 'alice_q', 'groups', 'indico', 'atlas_s', 'atlas_q', 'atlas_p', 'x_cvs', 'swlcg', 'gd', 'cernlib', 'compass_q', 'recover', 'tsm_recover', 'swlcg_p', 'swlcg_q', 'security_q', 'idle_projects', 'nobackup', 'compass_p', 'atlasT0', 'user_scratch', 'compass_s', 'older_exp_s', 'asis', 'afs', 'sysacct_s', 'users', 'obsolete_s', 'obsolete_p', 'older_exp', 'sys', 'cms_q', 'cms_p', 'egee', 'CALCA', 'shadow', 'obsolete_u', 'itprojects_p', 'itprojects_q', 'std', 'cms_s', 'CPHYS', 'work', 'alice', 'CCOMM', 'atlasTZ', 'l3c', 'afs_s', 'lhcb_p', 'lhcb_q'])

"""

# default location of central configuration file
CF = '/afs/cern.ch/project/afs/adm/afsadmin.cf'

import re

def read_cf(cf=CF,skip_comments=False):
    """Read the central CERN AFS configuration file, optionally skipping comment lines and always skipping empty (all whitespace) lines.
    Return a dictionary which is of form `{ section : [line1,line2,...] }`.
    """
    sections = {}
    current_section = None

    for line in open(cf):

        if not line.strip(): continue #skip empty or all-whitespace lines

        r = re.match(r'\[(\S+)\]', line)
        if r:
            current_section = r.group(1)
            sections.setdefault(current_section, [])
        else:
            if skip_comments and line[0] == '#': continue #skip comment lines

            sections[current_section].append(line)

    return sections



from cernafs.util import DataStruct, filter_items

class PartitionFeatures(DataStruct): 
    """PartitionFeatures contain the following attributes:

        - `servpart`: tuple (server,partition)
        - `pools`: list of pools assigned to the partition
        - `Xtags`: list of Xtags which are used for disabled pools or as special markers
        - `options`: list of options (containing '=' character)
    """
    pass
                
def partition_features(require=[],exclude=[]):
    """Return a list of :class:`PartitionFeatures` for all known partitions.
    The `require` and `exclude` options may contain a list of features (string tags) to constrain the returned list.
    """
    partitions = []

    for line in read_cf(skip_comments=True)['PARTITIONS']:
        tags = line.split()

        if not filter_items(tags, require, exclude):
            continue

        servpart = tuple(tags[0].split('/'))
        options = []
        pools = []
        Xtags = []

        # here are simple rules
        for x in tags[1:]:
            if '=' in x: # options contain '='
                options.append(x)
            else:
                if x and x[0]=='X': # anything else starting with X is a Xtag (used for disabled pools or as special markers)
                    Xtags.append(x)
                else:
                    pools.append(x) # everything else is a pool definition

        partitions.append(PartitionFeatures(servpart=servpart, pools=pools, Xtags=Xtags, options=options))

    return partitions

class ServerFeatures(DataStruct):
    """ ServerFeatures contain:

         - `name`: server name
         - `tags`: list of strings (tags)
    """


def server_features(require=[],exclude=[],prod=True):
    """Return a list of :class:`ServerFeatures` of known AFS servers. By default only those in production are listed (``prod=True``). If ``prod=False`` then
    *all* servers are listed. The `require` and `exclude` options may contain a list of features (string tags) to constrain the returned list.
    """

    if prod:
        exclude = exclude+['obsolete']  # TODO: if exclude.append("obsolete") then there is wierd side-effect in between calls to servers() !

    servers = []

    for line in read_cf(skip_comments=True)['AFSSERVERS']:

        tags = line.split()

        if filter_items(tags, require, exclude):
            servers.append(ServerFeatures(name=tags[0], tags=tags[1:]))

    return servers

def servers(require=[],exclude=[],prod=True):
    """Return a list of names of known AFS servers.  The meaning of the arguments is the same as for :func:`server_features`.
    """
    return [s.name for s in server_features(require, exclude, prod)]
    
# test / examples

if __name__=="__main__":
    fs = servers(require=["fs"])
    print(('File servers in production', len(fs), ':'))
    print(fs)
    print()
    prod = servers()
    print('Additional non-fs servers:')
    print((set(prod)-set(fs)))
    print(('All servers in production', len(prod)))
    print()
    all = servers(prod=False)    
    print('additional out-of-production servers:')
    print((set(all)-set(prod)-set(fs)))
    print(('All known servers', len(all)))


    print()
    parts = partition_features()

    print(('all partitions', len(parts)))
    print(('partitions with pools assigned', len([p for p in parts if p.pools])))
