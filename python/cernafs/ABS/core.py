import cernafs.util

import datetime
COMPACT_DATE_FORMAT = "%Y.%m.%d-%H%M%S" # compact format used for encoding metadata and identyfing dump sequences, preserves ASCII ordering

import logging
logger = logging.getLogger("core")


############
# Here we define which volumes are excluded from backup

def is_backupable(v):
   # only read-write volumes and take out the ones which are not backed up by definition
    return v.type=='RW' and v.name[0] not in ['R']

backupsys_xprefix = "'^R\.'"

############

class ABSException(Exception):
   pass

class ArchiveFailure(ABSException):
   """ Failure of archive command. The action indicates what the caller should do next:

      SKIP        - permanent error of this archive object (skip this archive object and proceed to the next one)
      RETRY       - transient object error which may be immediately recovered from, retry the command in the same object before proceeding to the next one
      RETRY_LATER - transient object error which may be recovered from in some time, proceed with another archive entry and retry the command on this object later
      ABORT       - permanent error for all objects, abort everything
   """

   ACTIONS = ['SKIP', 'RETRY', 'RETRY_LATER', 'ABORT']

   def __init__(self, s, action):
      self.s = s
      if action not in ArchiveFailure.ACTIONS:
          raise ValueError("Invalid action "+ str(action))
      self.action = action

   def __str__(self):
      return "%s: %s (%s)"%(self.__class__.__name__, self.s, self.action)


class VoldumpEntry(cernafs.util.DataStruct):
    """ A bunch of metadata attributes which describe the volume dump:
         - id (volume id)
         - name (volume name)
         - backupDate
         - previousBackupDate (None if full dump)
         - dataEncoding
         - chainId: identifier of the dump chain 

    //When the volume dump information is retrieved from the archive system, additional metadata specific to the archive system is stored as a separate data struct:
    //
    //     - archiveEntry (metadata specific to the entry in the archive backend)

    Ordering is given by backupDate.
    """

    def _filename(self): 
        "Encode a filename and return as string"
        fields = ['voldump']
        fields.append('v.'+str(self.id))
        fields.append('n.'+str(self.name))
        fields.append('b.'+self.backupDate.strftime(COMPACT_DATE_FORMAT))
        fields.append('p.'+ ('0' if not self.previousBackupDate else self.previousBackupDate.strftime(COMPACT_DATE_FORMAT)))
        fields.append('e.'+self.dataEncoding)
        fields.append('i.'+str(self.chainId))
        return ":".join(fields) + ":"

    @classmethod
    def _scan_filename(self, fn): 
        "create a DumpName object out of encoded filename fn "
        
        fields = fn.split(":")

        if fields[0][-7:] != 'voldump':
            raise ValueError(fn)

        e = VoldumpEntry()

        e.id = fields[1][2:]
        e.name = fields[2][2:]
        e.backupDate = datetime.datetime.strptime(fields[3][2:], COMPACT_DATE_FORMAT)
        s = fields[4][2:]
        if s == '0':
            e.previousBackupDate = None
        else:
            e.previousBackupDate = datetime.datetime.strptime(s, COMPACT_DATE_FORMAT)
        e.dataEncoding = fields[5][2:]
        e.chainId = fields[6][2:]

        # there is a workaround for ca 2014 filenames in CASTOR which did not have the : separator and which
        # ended up like this: ...:i.2014.07.08-163728aes-v2
        # checked 2022: we still have some of these.
        if 'aes-v' in e.chainId:
            logger.info("workaround: found old CASTOR filename without proper enc separator: %s" % (fn))
            e.chainId = e.chainId.replace('.aes-v1', '')
            e.chainId = e.chainId.replace('.aes-v2', '')
            e.chainId = e.chainId.replace('aes-v1', '')
            e.chainId = e.chainId.replace('aes-v2', '')

        return e

    def __lt__(self, other):
        return self.backupDate < other.backupDate

def find_voldump_chain(voldumps,datestamp=None):
   """ Find the most recent chain (time-sorted list) of voldumps not older than datestamp.
   If the datestamp is not specified (None) then find the chain leading to the most recent dump in voldumps.
   In case of errors (e.g. broken chain) return an empty list.
   """

   most_recent_chain = []

   # first element is oldest
   # last element is most recent
   voldumps = sorted(voldumps)

   #for vd in voldumps:
   #   print "---",vd.backupDate,vd.previousBackupDate

   current_dump = None

   if datestamp is None:
       try:
           current_dump = voldumps[-1] # most recent dump
       except IndexError:
           return []
   else:
       for i in range(len(voldumps)):
           if voldumps[i].backupDate > datestamp:
               break
           current_dump = voldumps[i]

   dump_dictionary = {}

   for d in voldumps:
      if d.backupDate in dump_dictionary:
         # duplicate key
         logger.warning("Duplicate dump of %s on %s (same backupDate key in the voldump chain).  \n entry1: %s \n entry2: %s", d.name, d.backupDate, str(d), str(dump_dictionary[d.backupDate]))
      dump_dictionary[d.backupDate] = d


   #for d in dump_dictionary:
   #    print "dump_dictionary", d, dump_dictionary[d]

   already_visited = set() # avoid recursion

   while current_dump:
      #print "current_dump",current_dump.backupDate,current_dump.previousBackupDate
      most_recent_chain.append(current_dump)
      if current_dump.previousBackupDate is None:
         current_dump = None
      else:
         try: 
             current_dump = dump_dictionary[current_dump.previousBackupDate]
         except KeyError:
             # this is a broken chain (the parent dump does not exist) so we return an empty list
             logger.error("Broken chain on %s (parent dump does not exist): %s", current_dump.backupDate, current_dump)

             logger.error("Trying to skip the dump... finding previous parent...")
             # try to find the previous backup dump (skip missing parent...)
             for i in range(len(voldumps)):
                #print i,voldumps[i]
                if voldumps[i].backupDate == current_dump.backupDate:
                   if i>0:
                      #FIXME: here we need to add an extra protection that will prevent to "cross-link" two independent chains in case the full dump is the missing one
                      #The check may be implemented by comparing the identifier of the dump chain (chainId may be used for that)

                      current_dump2 = voldumps[i-1]

                      if current_dump.chainId != current_dump2.chainId:
                         raise ABSException("Broken chain for %s on %s -- unable to find the parent dump in the same backup chain %s)"%(current_dump.name, current_dump.previousBackupDate, current_dump.chainId))

                      current_dump = current_dump2

                      logger.error("Skipping missing dump... now at %d %s", i, current_dump)
                      break
                   else:
                      raise ABSException("Broken chain for %s on %s -- unable to find the parent dump ('bad luck filling up the hole...')"%(current_dump.name, current_dump.previousBackupDate))


         if current_dump.backupDate in already_visited:
            # with a minute resolution a dump may be referencing itself if created within the same minute twice
            logger.error("Infinite recursion in the voldump chain (will not further analyze this backup chain and will return an empty list) -- this voldump was already visited: %s", current_dump)
            return []
         already_visited.add(current_dump.backupDate)


   return most_recent_chain

import re
def find_string_in_file(s, f):
    for line in f:
        if re.search(s, line):
            return True
    return False
