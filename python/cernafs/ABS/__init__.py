import sys, os.path

from cernafs.script import rundir

# begin: modules exposed to config options
import datetime
import random
# end: modules exposed to config options

_options = None
def get_config_options(config_fn=None):
    import logging
    logger = logging.getLogger("ABS")

    global _options
    if _options is not None:
        return _options

    if config_fn is None:
        config_fn = rundir('abs.config')

    logger.debug('Reading config options from file %s', config_fn)
    try:
        with open(config_fn, 'r') as config:
            _options = eval(config.read())
            logger.debug('Config options: %s',repr(_options))
            return _options
    except Exception as x:
        logger.warning("Cannot read the config options file %s %s", config_fn, x)
        raise x
    
# import script # this is to store the original executable path

# id = None
# logdir = None
# logger = None
# name = None

# def initialize(_name):
#     global id,logdir,logger,name

#     name = _name
#     id = datetime.datetime.now().strftime(COMPACT_DATE_FORMAT)
#     logdir = '/tmp/ABS/backup-session.%s'%backup_session_id
