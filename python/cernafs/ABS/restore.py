
#
# high-level restore functions
# used by abs-restore, abs-mass-restore, ...
# these functions are using lower level archive subsystem interface

# TODO: implement restore from multiple archives

from cernafs.ABS.core import find_voldump_chain
from cernafs.ABS.archiveCTA import archive_query, archive_prepare_bulk, archive_poll_for_offline_file, archive_get_pipetransfer_cmd
import cernafs.util
from cernafs import vos
import os.path, os 
import datetime

import logging

logger = logging.getLogger("restore")

def get_restore_server_partition(assure_this_server=True):
    import cernafs.cf
    import sys

    abs_restore_tag = [p.servpart for p in cernafs.cf.partition_features(require=['absrestore'])]
    abs_restore_server_tag = [p.name for p in cernafs.cf.server_features(require=['absrestore'])]

    if len(abs_restore_tag)>1 or len(abs_restore_server_tag)>1:
        logger.critical("multiple absrestore partitions/servers defined in afsadmin.cf (should be one only): %s %s", abs_restore_tag, abs_restore_server_tag)
        sys.exit(2)

    if len(abs_restore_tag)==0 or len(abs_restore_server_tag)==0:
        logger.critical('no absrestore partition/server defined in afsadmin.cf',)
        sys.exit(2)

    restore_server, restore_partition = abs_restore_tag[0]

    if restore_server != abs_restore_server_tag[0]:
        logger.critical("conflicting defintion of absrestore in [server] and [partitions] section of afsadmin.cf: %s %s", abs_restore_tag, abs_restore_server_tag)
        sys.exit(2)

    # limit the abs-restore functionality to the restore server only
    if assure_this_server:
        if not cernafs.util.same_hostname(restore_server, cernafs.util.hostname()):
            logger.critical("trying to restore on a server (%s) which does not match absrestore tag in afsadmin.cf (%s)", cernafs.util.hostname(), restore_server)
            sys.exit(2)

    return restore_server, restore_partition

def vos_restore_from_archive(volid, datestamp, server, restore_partition, logdir, vol_restore_name):
    """ If vol_restore_name is None then it will be generated automatically
    """
    archive_dumps = archive_query(volid)
    backup_chain = find_voldump_chain(archive_dumps, datestamp)
    backup_chain.reverse() # oldest (i.e. full) first

    if not backup_chain:
        logger.warning("Nothing in the archive to restore for volume %s at %s", volid, datestamp)
        return

    # the restore volume name:
    # R.<volid>.<MONTH><DAY><HOUR><MINUTE>
    # this should be sufficient to track back the original volume id/name, give an idea about the restore time and provide a unique restore identifier
    # this means that the second restore within the same minute would fail, so we will add an extra digit in case of conflict
    if not vol_restore_name:
        vol_restore_name = '.'.join(["R", volid, datetime.datetime.now().strftime("%m%d%H%M")])

    # check if restore volume exists and give back a nice message - but we don't know whether this is mounted.
    try:
        v = vos.examine(vol_restore_name, silent=True)
        if v.id:
            logger.warning("The to-be restored volume %s already exists", vol_restore_name)
            return vol_restore_name
    except cernafs.util.CachedCallError:
        pass

    logger.info("Restoring volume %s at %s, recalling %d dumps", volid, datestamp, len(backup_chain))
    archive_prepare_bulk([x.archiveEntry.filename for x in backup_chain])

    incremental = False
    i = 0
    counter = 0
    for enc, fn in [(x.dataEncoding, x.archiveEntry.filename) for x in backup_chain]:
       i += 1
       logger.info("Waiting for dump %d to arrive", i)
       archive_poll_for_offline_file(fn)
       logger.info("Starting to restore dump %d", i)
       transfer_cmd = archive_get_pipetransfer_cmd(fn)
       log_name = os.path.basename(fn)
       counter = vos_restore_pipe(server, restore_partition, vol_restore_name, transfer_cmd, log_name, incremental, enc, logdir, counter)
       incremental = True

    return vol_restore_name+str(counter)


def vos_restore_pipe(server,partition,volname,pipe_cmd,log_name,incremental,data_encoding,logdir, counter=0):
    logger.debug('vos_restore: %s %s %s %s %s %s %s', server, partition, volname, pipe_cmd, log_name, incremental, data_encoding)

    transfer_cmd = ' '.join(pipe_cmd) + ' |'

    decode_cmd = ''

    for enc in reversed(data_encoding.split(".")):

        if enc == "gzip":
            decode_cmd += ' gunzip -c |'
            continue

        if enc.startswith("aes"):
            decode_cmd += " openssl enc -d -aes-256-cbc -pass 'file:/var/ABS/private/pw.%s'  -md md5 |" %enc
            continue

        raise Exception('Unsupported data encoding "%s" (%s)'%repr(enc, data_encoding))

    while True:
        try:
            restore_name = volname + str(counter)
            restore_cmd = 'vos restore -server "%s" -partition "%s" -name "%s"'  % (server, partition, restore_name)
            if incremental:
                restore_cmd += ' -overwrite i'


            cmd = transfer_cmd + decode_cmd + restore_cmd
            cmd_log = os.path.join(logdir, log_name)
            cernafs.util.cached_call(cmd, shell=True, cache_expired=True, cache=True, cache_path=cmd_log, cache_auto_fn=False, silent=True)
            logger.info("%s progress : one %s restore done" %(volname, 'incremental' if incremental else 'full'))
            return counter
        except Exception as e:
            counter = counter + 1
            if counter > 9:
                raise e
