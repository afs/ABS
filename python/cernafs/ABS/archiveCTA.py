
import time
import os.path
import re

import cernafs.util
import datetime

from .core import VoldumpEntry, ArchiveFailure

import logging

logger = logging.getLogger("archiveCTA")

tape_server = 'eosctabackup'
tape_pollinterval = 60
config_options = None
logdir = None

TAPE_PREFIX = '/eos/ctabackup/archive/backup/afsback/DUMPS'

ID_HASH_RANGE_1 = 50
ID_HASH_RANGE_2 = 50


def archive_prepare_bulk(archive_fns):
    """ trigger efficient recall=stage-in of all needed files. On return, the data will (probably) not yet be on disk,
    still need to wait for files
    """
    xrdcp_host = config_options.get('xrdcp_host', tape_server)
    for fn in archive_fns:
        cernafs.util.cached_call(['xrdfs', xrdcp_host, 'prepare', '-s', _tape_voldump_path(fn)])

def archive_poll_for_offline_file(fn):
    xrdcp_host = config_options.get('xrdcp_host', tape_server)
    xrd_poll_for_offline_file(xrdcp_host, _tape_voldump_path(fn))

def archive_get_pipetransfer_cmd(fn):
    xrdcp_host = config_options.get('xrdcp_host', tape_server)
    return ['xrdcp',
            '-d', '1', '--posc', '--nopbar',
            "root://%s/%s" % (xrdcp_host, _tape_voldump_path(fn)),
            '-']

def _tape_voldump_path(volid, fn=""):
    # two-level caching identical to CatalogCache structure
    path = os.path.join(TAPE_PREFIX,  str(int(cernafs.util.uniform_hash(volid)*ID_HASH_RANGE_1)),  str(int(cernafs.util.uniform_hash(volid+'x')*ID_HASH_RANGE_2)), str(volid))

    # Otherwise we joing with empty, creating a trailing /
    if fn:
        return os.path.join(path, fn)
    return path


def archive_initialize(config_opts, _logdir):

   global config_options, logdir

   config_options = config_opts
   logdir = _logdir


class CTAArchiveFileEntry(cernafs.util.DataStruct):
   """ File metadata stored in the tape archive:
         * size (integer, number of bytes)
         * archiveDate (datetime)
         * filename (string)
         * mbit (bool): migration to tape bit
   Natural sorting by archiveDate.
   """

   def __cmp__(self, other):
       return cmp(self.archiveDate, other.archiveDate)

   def __str__(self):
       return ('%s %s %12l %s' % ('m' if self.mbit else '-', str(self.archiveDate), self.size, self.filename))


def archive_delete_bulk(voldumps,**cached_call_opts):

    xrdcp_host = config_options.get('xrdcp_host', tape_server)
    for d in voldumps:
        cnt = 1
        while cnt<=3:
            try:
                cernafs.util.cached_call(['xrdfs', xrdcp_host, 'rm', d.archiveEntry.filename], check_exception=True)
                break
            except cernafs.util.CachedCallError as x:
                logger.warning('xrdfs rm failed: attempt %d: %s %s %s', cnt, d.archiveEntry.filename, x.result.stdout, x.result.stderr)
                cnt += 1

def archive_list_volume_ids(range1=list(range(ID_HASH_RANGE_1)), range2=list(range(ID_HASH_RANGE_2))):
    vids = []

    xrdcp_host = config_options.get('xrdcp_host', tape_server)

    for i1 in range1:
        for i2 in range2:

            path = os.path.join(TAPE_PREFIX, str(i1), str(i2))

            try:
                r = cernafs.util.cached_call(['xrdfs', xrdcp_host, 'ls', path], check_exception=True)
            except cernafs.util.CachedCallError as x:
                skip = False
                for line in x.result.stderr:
                    if '[ERROR]' in line:
                        skip = True
                        break
                if skip:
                    continue
                else:
                    raise

            vids += [line.strip() for line in r.stdout]

            logger.debug('%d vids after adding path %s', len(vids), path)

    return vids


def archive_query(volid,**cached_call_opts):
    xrdcp_host = config_options.get('xrdcp_host', tape_server)

    try:
        r = cernafs.util.cached_call(['xrdfs', xrdcp_host,
                                      'ls', '-l', _tape_voldump_path(volid)],
                                     check_exception=True, **cached_call_opts)
    except cernafs.util.CachedCallError as x:
        for line in x.result.stderr:
            if 'No such file or directory' in line:
                return []

        raise ArchiveFailure("unrecognized error volid=%s exc=%s"%(volid, str(x)), 'SKIP')

    entries = []
    for line in r.stdout:

        #  each line of 'xrdfs' output contains a plain file name
        try:
            # OLD EOS 4 format
            # $ xrdfs castorpublic ls -l /castor/cern.ch/user/i/iven
            #-r-- 2013-11-22 16:34:11         978 /castor/cern.ch/user/i/iven/c2ppstest
            #-r-- 2015-04-22 13:59:33           0 /castor/cern.ch/user/i/iven/cc7-group
            #-r-- 2015-04-22 13:59:47           0 /castor/cern.ch/user/i/iven/test_cc7-group
            #dr-x 2018-08-30 16:28:14           0 /castor/cern.ch/user/i/iven/tmp
            #d--- 2015-06-15 12:05:40           0 /castor/cern.ch/user/i/iven/tmp-new
            #dr-x 2010-01-27 08:04:32           0 /castor/cern.ch/user/i/iven/tmp-old
            #-r-- 2018-02-14 07:51:54   664780234 /castor/cern.ch/user/i/iven/work.tgz
            #-r-- 2018-02-14 08:01:31   791949866 /castor/cern.ch/user/i/iven/work.zip

            # NEW EOS 5 format
            # $ xrdfs eosctabackup.cern.ch ls -l /eos/ctabackup/archive/...
            #-rw-r--r-- uuuafs def-cg 225813920 2023-09-06 16:51:22 /eos/ctabackup/a....

            line = line.strip()
            fields = line.split()
            archive_entry = CTAArchiveFileEntry(size=0, archiveDate=datetime.datetime.now())
            archive_entry.filename = _tape_voldump_path(volid, fields[6])
            archive_entry.size = int(fields[3])
            archive_entry.mbit = False  ## not reported by 'xrdfs ls', would need to 'xrdfs stat' each file - not worth it.
            voldump_entry = VoldumpEntry._scan_filename(archive_entry.filename)
        except (ValueError, IndexError) as x:
            logger.warning("%s: could not parse line '%s', ignored entry %s" %(x, line, archive_entry))
            continue
        voldump_entry.archiveEntry = archive_entry
        entries.append(voldump_entry)

    if not entries:
        logger.debug("archive_query(%s): no results", volid)
    else:
        logger.debug("archive_query(%s): %d entries found", volid, len(entries))
    return entries


def archive_streamdump_prepare_process(volid, filename):
    # create the target dump directory if it does not exist

    xrdcp_host = config_options.get('xrdcp_host', tape_server)

    cernafs.util.cached_call(['xrdfs', xrdcp_host, 'mkdir', '-p', '-mrwx------', _tape_voldump_path(volid)])

    command = "xrdcp --posc --nopbar - 'root://%s/%s'"%(xrdcp_host, _tape_voldump_path(volid, filename))
    process_name = "xrdcp"
    environ = {}

    return (command, process_name, environ)

def archive_streamdump_check_process(exitcode, logfile, volid, filename):
    # returns "arch_bytes"
    # raise ArchiveFailure if errors detected
    arch_bytes = 0
    if exitcode != 0:
        raise  ArchiveFailure("Non-zero exit code from xrdcp (%d) for volid %s"%(exitcode, volid), "RETRY")

    xrdcp_host = config_options.get('xrdcp_host', tape_server)

    try:
        r = cernafs.util.cached_call(['xrdfs', xrdcp_host, 'stat', _tape_voldump_path(volid, filename)], check_exception=True)
        g = re.search(r'Size:\s+(\d+)', r.stdout.read())
        if g:
            arch_bytes = int(g.group(1))
    except cernafs.util.CachedCallError:
        raise ArchiveFailure("File not found after dump, xrdfs stat: %s"%_tape_voldump_path(volid, filename), "RETRY")

    # no need to set per-file permissions, they inherit from the parent (which is locked down)

    for line in open(logfile):
        if '[ERROR]' in line:
            raise ArchiveFailure("xrdcopy [ERROR] found in logfile: %s" % repr(line), "RETRY")
    return arch_bytes

def archive_streamdump_cleanup(volid, filename):
    """ Cleanup after a failed transfer.
    """
    p = _tape_voldump_path(volid, filename)
    xrdcp_host = config_options.get('xrdcp_host', tape_server)
    cernafs.util.cached_call(['xrdfs', xrdcp_host, 'rm', p])

def xrd_poll_for_offline_file(xrdcp_host, fn):
    """ Wait for data to be on disk - simply staying blocked in 'xrdcp' no longer works
    """
    while True:
        result = cernafs.util.cached_call(['xrdfs', xrdcp_host, 'stat', '-q', 'Offline', _tape_voldump_path(fn)],
                                              silent=True, check_exception=False)
        if result.returncode == 55:  # Online: [ERROR] Query response negative
            break
        elif result.returncode == 0: # still Offline
            time.sleep(tape_pollinterval)
        else:
            logger.error("Calling stat on  %s failed with return code %s", fn, result.returncode)
            raise result
