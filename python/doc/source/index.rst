.. CERNAFS documentation master file, created by
   sphinx-quickstart on Tue Sep 27 14:24:24 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CERNAFS's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2

   concepts
   ref

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

